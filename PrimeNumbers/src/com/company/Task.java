package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Task implements Callable<List<Integer>> {
    private List<Integer> allNums;
    public Task(List<Integer> allNums ) { this.allNums = allNums; }

    @Override
    public List<Integer> call() throws Exception {
        List<Integer> result = new ArrayList<>();

        for ( Integer num:allNums ) {
            boolean isPrime = isPrime(num);
            if (isPrime){
                result.add(num);
            }
        }

        return result;
    }

    private boolean isPrime(Integer n) {

            // Check if number is 2
        if (n == 2)
            return true;

            // Check if n is a multiple of 2
        if (n % 2 == 0)
            return false;

        // If not, then just check the odds
        for (int i = 3; i <= Math.sqrt(n); i += 2){
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
