package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    public static void main(String[] args) {
        List<Integer> randomNumbers = createRandomNumberArray(10000);
        System.out.println(randomNumbers);

        // Create a thread pool
        ExecutorService service = Executors.newCachedThreadPool();

        // Returns a list of future results and submits the tasks
        List<Future> allFutures = taskSubmit(service, randomNumbers,100);

        //Results
        List<Integer> resultArray = returnResultArray(allFutures);

        // shutdowns the service
        service.shutdown();

        System.out.println(resultArray.size());
        System.out.println(randomNumbers.size());
    }

    private static List<Integer> returnResultArray(List<Future> allFutures) {
        List<Integer> resultArr = new ArrayList<>();
        int i = 0;
        while ( i < allFutures.size() ){
            Future<List<Integer>> future = allFutures.get(i);
            try {
                List<Integer> result = future.get();
//                System.out.println("Result No " + i + " = " + result.get(0));
                resultArr.addAll(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            i++;
        }

        return resultArr;
    }

    private static List<Future> taskSubmit(ExecutorService service, List<Integer> randNumsArr, int eToCKPerThread) {
        if (eToCKPerThread > randNumsArr.size()){
            System.out.println("elements to check per thread is greater than the size of the array");
            return null;
        }

        List<Future> allFutures = new ArrayList<>();
        int numOfThreads = randNumsArr.size() / eToCKPerThread;
        for (int i = 0; i < numOfThreads; i++){
            int startIdx = i*eToCKPerThread;
            int endIdx = startIdx + eToCKPerThread;
            Future future = service.submit(new Task(randNumsArr.subList(startIdx,endIdx)));
            allFutures.add(future);
        }

        return  allFutures;
    }

    private static List<Integer> createRandomNumberArray( int lengthOfArray){
        List<Integer> result = new ArrayList<>();
        for ( int i = 0; i < lengthOfArray; i++ ){
            Random rand = new Random();
            int randInt = rand.nextInt(1000000);
            result.add(randInt);
        }
        return result;
    }
}
