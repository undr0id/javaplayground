package com.company;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        // Creates and fills toBeCopied array
        List<List<String>> toBeCopied = createCopyArray();

        // Creates the pool
        ExecutorService service = Executors.newCachedThreadPool();

        // Creates a list of futures and submits tasks
        List<Future> allFutures = getAllFutures(toBeCopied, service);

        //Results
        List<List<String>> resultArr = getResultArray(allFutures);

        // Shutsdown the thread pool
        service.shutdown();
    }

    private static List<List<String>> getResultArray(List<Future> allFutures) {
        List<List<String>> resultArr = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Future<List<List<String>>> future = allFutures.get(i);
            try {
                List<List<String>> result = future.get();
                resultArr.addAll(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        return  resultArr;
    }

    @NotNull
    private static List<Future> getAllFutures(List<List<String>> toBeCopied, ExecutorService service) {
        List<Future> allFutures = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            int startIdx = i*100;
            int endIdx = startIdx + 100;
            Future<List<List<String>>> future = service.submit(new Task(toBeCopied.subList(startIdx,endIdx)));
            allFutures.add(future);
        }
        return allFutures;
    }

    private static List<List<String>> createCopyArray() {
        List<List<String>> toBeCopied = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            List<String> l = new ArrayList<>();
            for (int j = 0; j < 1000; j++) {
                l.add("Test");
            }
            toBeCopied.add(l);
        }
        return toBeCopied;
    }
}
