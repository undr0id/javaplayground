package com.company;

import java.util.List;
import java.util.concurrent.Callable;

// Zashto ne moga da napravq tozi task static
class Task implements Callable<List<List<String>>> {
    private List<List<String>> subList;

    public Task(List<List<String>> subList) { this.subList = subList; }

    @Override
    public List<List<String>> call() throws Exception {
        System.out.println(Thread.currentThread().getName());
        return subList;
    }
}
