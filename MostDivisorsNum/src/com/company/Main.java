package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] shah) {
        // Create a thread pool
        ExecutorService service = Executors.newCachedThreadPool();

        // Returns a list of future results and submits the tasks
        List<Future> allFutures = taskSubmit(service, createArray(1000),100);

        //Results
        List<Integer> resultArray = returnResultArray(allFutures);

        // shutdowns the service
        service.shutdown();

        System.out.println( resultArray.size() );
        System.out.println( resultArray.get(0) );
        System.out.println( resultArray.get(1) );
        System.out.println( resultArray );
    }

    private static List<Integer> returnResultArray(List<Future> allFutures) {
        List<Integer> result = new ArrayList<>();
        result.add(0);
        result.add(0);

        int i = 0;
        while ( i < allFutures.size() ){
            Future<List<Integer>> future = allFutures.get(i);
            try {
                List<Integer> currThreadResult = future.get();

                if ( result.get(0) < currThreadResult.get(0) ){
                    result.set(0, currThreadResult.get(0) );
                    result.set(1, currThreadResult.get(1) );
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            i++;
        }

        return result;
    }

    private static List<Future> taskSubmit(ExecutorService service, List<Integer> numArray, int eToCKPerThread) {
        if (eToCKPerThread > numArray.size()){
            System.out.println("elements to check per thread is greater than the size of the array");
            return null;
        }

        List<Future> allFutures = new ArrayList<>();
        int numOfThreads = numArray.size() / eToCKPerThread;
        for (int i = 0; i < numOfThreads; i++){
            int startIdx = i*eToCKPerThread;
            int endIdx = startIdx + eToCKPerThread;
            Future future = service.submit(new Task(numArray.subList(startIdx,endIdx)));
            allFutures.add(future);
        }

        return  allFutures;
    }

    private static List<Integer> createArray( int lengthOfArray ){
        List<Integer> result = new ArrayList<>();
        for ( int i = 1; i < lengthOfArray; i++ ){
            result.add(i);
        }
        return result;
    }
}
