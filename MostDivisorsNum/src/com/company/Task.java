package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Task implements Callable<List<Integer>> {
    private List<Integer> numArr;

    public Task(List<Integer> numArr){ this.numArr = numArr; }

    @Override
    public List<Integer> call() throws Exception {
        return checkDivisors();
    }

    private List<Integer> checkDivisors() {
        List<Integer> result = new ArrayList<>();
        result.add(0);
        result.add(0);

        for (int i = 0; i < numArr.size(); i++) {
            int prevDivisorCounter = 0;
            int currDivisorCounter = 0;
            int num = numArr.get(i);

            for (int j = 1; j < num; j++) {
                if (num % j == 0){
                    currDivisorCounter++;
                }
            }

            if (currDivisorCounter >= prevDivisorCounter){
                result.set(0,currDivisorCounter);
                result.set(1,num);
                prevDivisorCounter = currDivisorCounter;
            }
        }

        return result;
    }
}
